# Nest js migrations and seeders

## Install Dependencies

```
 npm install
```

## Copy migrations and seeders config files

```
    cp ormconfig.ts.example ormconfig.ts
    cp seederconfig.ts.example seederconfig.ts
    cp .env.example .env
```

## Configuration

Configure ormconfig.ts, seederconfig.ts and .env with your database credential

## Migration and Seeder comands

```
npm run migration:generate db/migrations/{migration_name}
npm run migration:create
npm run migration:run
npm run migration:revert
npm run migration:refresh // drop database and run migrations
npm run migration:seed // drop database, run migrations and run seeders
```

import { DataSource } from 'typeorm';
import { Seeder, SeederFactoryManager, runSeeder } from 'typeorm-extension';
import { UserSeeder } from './users.seeder';
import { EstadoSeeder } from './estados.seeder';

export default class InitSeeder implements Seeder {
  public async run(
    dataSource: DataSource,
    factoryManager: SeederFactoryManager,
  ): Promise<any> {
    await runSeeder(dataSource, EstadoSeeder);
    await runSeeder(dataSource, UserSeeder);
  }
}

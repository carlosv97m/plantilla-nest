export const config = () => ({
  database: {
    type: process.env.DB_TYPE,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    entities: ['dist/src/**/**/*.entity{.js,.ts}'],
    migrations: ['dist/db/migrations/*{.js,.ts}'],
    synchronize: false,
    logging: process.env.LOGGING,
  },
});
